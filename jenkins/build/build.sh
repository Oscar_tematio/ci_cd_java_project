#!/bin/bash

#copy the jar to the build location

cp -f  java-app/target/*.jar  jenkins/build/

echo '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*'
echo '*-*-*-Building docker image-*-*-*-*'
echo '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*'
# figlet -f slant "building the docker image "

cd jenkins/build/ && docker-compose -f docker-compose-build.yml build --no-cache
