# echo "--*-**-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-"
# echo "-*-*-*-*-*-*-*-*Building the JAR-**-*-*-*-*-*-"
# echo "--*-**-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-"

WORKSPACE=/home/oscar/WS/jenkins/jenkins-data/jenkins_home/workspace/pipeline-docker-maven

docker run --rm -v $WORKSPACE/java-app:/app -v /root/.m2:/root/.m2 -w /app  maven:3-alpine "$@"