#!/bin/bash

echo "--*-**-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-"
echo "-*-*-*-*-*-*-*-Pushing the image-**-*-*-*-*-*-"
echo "--*-**-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-"

IMAGE="maven-poject"

echo "-*-*-*-*-*-*-*-Logging to the docker hub-**-*-*-*-*-*-"

docker login -u oscartematio -p $PASS

echo "-*-*-*-*-*-*-*-Tagging the image -**-*-*-*-*-*-"

docker tag $IMAGE:$BUILD_TAG   oscartematio/$IMAGE:$BUILD_TAG

echo "-*-*-*-*-*-*-*-Pushing the image-**-*-*-*-*-*-"

docker push oscartematio/$IMAGE:$BUILD_TAG